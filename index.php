<html>
<head>
    <?php
    ######################
    $licenseAccepted = false; #You have to accept the license (Appache 2.0 https://www.apache.org/licenses/LICENSE-2.0) before using the software!
    $author = "Cripi on Github | https://github.com/cripiLP/myhost"; #Set the author, if you want you can cretid me (Cripi) together with the github repo (https://github.com/cripiLP/myhost).
    $pagetitle = "Selfmade Bildhoster | available on GitHub"; #Here you can enter your Page title
    $canFoundByGoogle = false; #If google can find you, set this var on true and you must enter a google-site-verification code
    $googleSiteVerificationCode = "";#Your google site verification code
    $useRecaptcha = false; #Set this on true if you will use googles recaptcha. You must insert your data-site-key and you Secret
    $recaptchaDataSiteKey = ""; #Your google recaptcha data-site-key
    $recaptchaSecret = ""; #Your google recaptcha Secret
    $stopScriptAtError = true; #Set this on true to stop the script after a Error was detectet. Fatal errors like missing files are stoped allways
    $path = 'upload/img/'; #Folder for uploadet Pictures
    $unallowed = array( #Images that are not allowed to be shown
        '',
    );
    $errorPicture = "error.jpg"; #Picture that´s shown when a unknown picture in the Preview is requested (Relative to the Path)
    $previewPicture = "logo.jpg"; #Picture that´s shown when the website picture in the Preview is requested (Relative to the Path)
    $forbiddenPicture = "403_error.png"; #Picture that´s shown when a forbidden picture in the Preview is requested (Relative to the Path)
    $blockedIps = file('blockedIps.txt'); #If you will block an Ip (Maybe the Ip of a flooder) insert the IP here
    $maxImgStorage = 1024; #The max size that can used for Picture storage. When this limit is reached, then pictures still can be uploadet, but it will give you a error and sends a mail to you that your planed storage is full! (This limit can exceeded maximal by the size of one image!)
    $max_filesize = 4; #Set the Max filesize in MB
    $newNameLength = 5; #How long should the new filename be? Calc the amount of different filenames with 62^newNameLength
    $allowedTypes = array( #Set the Allowed Types for a image File
        'png',
        'jepg',
        'gif',
        'jpg',
        'jpeg'
    );
    ######################
    $maxFilesizeMB = $max_filesize;
    $max_filesize *= 1048576; #Multiply the filesize with factor MB
    if ($licenseAccepted) {
    try { #Icludes the functions and the msgout class, if error send a mail to the serrver_admin
        require 'functions.php';
        require 'msgout.php';
    } catch (Exception $exception) { #Catch the exception and send mail
        create_log("catched_exception", "Today at " . date('l jS \of F Y h:i:s A') . " a Exception on Line " . __LINE__ . " was Catched. The Exception is: " . $exception);
        mail($_SERVER['SERVER_ADMIN'], "Cached Exception", "Today at " . date('l jS \of F Y h:i:s A') . " a Exception on Line " . __LINE__ . " was Catched. The Exception is: " . $exception, "Fromm: Script <script@" . $_SERVER['HTTP_HOST'] . ">"); #Send mail to Server owner if server cannot include the functions.php
        die("<h1>500 Error | Server Admin is informed!</h1>"); #Stopping Script
    }
    $msg = new msgout; #Create class for msgout
    $url = "https://www.google.com/recaptcha/api/siteverify?secret=" . $recaptchaSecret . "&response=" . null;
    if ($useRecaptcha && !empty($recaptchaDataSiteKey) && !empty($recaptchaSecret)) { #Check do recaptcha should be used and do site key and secret is not empty
        $curl = curl_init($url); #Initialise curl object for check the secret
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); #Set the CURLOPT_FOLLOWLOCATION optin on true
        curl_setopt($curl, CURLOPT_FRESH_CONNECT, true); #Set CURLOPT_FRESH_CONNECT optin on true
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); #Set the CURLOPT_RETURNTRANSFER on true
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); #Set the CURLOPT_SSL_VERIFYPEER on true
        $json = json_decode(utf8_decode(curl_exec($curl)), true); #Execute the curl request and decode the Json for check
        if (isset($json['error-codes']['1'])) { #If issed the errorcode that say that the Secret is wrong, stop the Script and send a mail to the webmaster
            create_log("bad_recapcha _secret", "Today at " . date('l jS \of F Y h:i:s A') . " was detected that your google recaptcha is wrong!");
            mail($_SERVER['SERVER_ADMIN'], "Bad recapcha Secret", "Today at " . date('l jS \of F Y h:i:s A') . " was detected that your google recaptcha is wrong!", "Fromm: Script <script@" . $_SERVER['HTTP_HOST'] . ">"); #Send mail to Server owner if server cannot include the functions.php
            if ($stopScriptAtError) { #When stopScriptAtError then stop the Script with a errpr message
                die("<h1>500 Error | Server Admin is informed!<h1>");
            } else {#When stopScriptAtError is false set form without recaptcha
                $form = '<form enctype="multipart/form-data" action="" method="post"><input type="file" name="img><br><input type="submit" value="Hochladen"><br></form>'; #set form
                $useRecaptcha = false;
            }
        } else { #If all fine, set the Form with the recaptcha and include the recaptcha api
            echo "<script src='https://www.google.com/recaptcha/api.js'></script>\n";
            $form = '<form enctype="multipart/form-data" action="" method="post"><input type="file" name="img"><br><div style="display: inline-block;"><div class="g-recaptcha" data-sitekey="' . $recaptchaDataSiteKey . '" data-theme="light"></div></div><br><input type="submit" value="Hochladen"><br></form>';
        }
    } else { #If use recaptcha false ore not both fields filled, set the Form withoud the recaptcha
        $form = '<form enctype="multipart/form-data" action="" method="post"><input type="file" name="img"><br><input type="submit" value="Hochladen"><br></form>';
        $useRecaptcha = false;
    }
    $inUpload = false;
    echo "<title>" . $pagetitle . "</title>\n"; #Give out the Pagetitle
    echo "<meta charset=\"utf-8\">\n"; #set the Charset to utf8
    echo "<link rel=\"stylesheet\" href=\"style/style1.css\">\n"; #include the  Stylesheet
    #echo "<link type=\"text/plain\" rel=\"author\" href=\"" . $_SERVER['HTTP_HOST'] . "/humans.txt\" />"; #Link the humans.txt (currently not existing, planed in future)
    $author = "<meta name=\"author\" content=\"" . $author . "\">\n"; #Set the author, if you want you can cretid me (Cripi) together with the github repo (https://github.com/cripiLP/img_upload).
    echo "<meta http-equiv=\"Content-Language\" content=\"de\"/>\n"; #Set content language to De (Deutsch/German)
    if ($canFoundByGoogle && !empty($googleSiteVerificationCode)) { #Whem google verification used an googleSiteVerificationCode is filled echo this
        echo "<meta name=\"google-site-verification\" content=\"" . $googleSiteVerificationCode . "\">\n";
    }
    echo "<meta name=\"date\" content=\"" . date('l jS \of F Y h:i:s A') . "\">\n"; #Set date of content
    echo "<meta property=\"og:site_name\" content=" . $_SERVER['HTTP_HOST'] . ">\n"; #Set sitename to the host
    if (isset($_GET['i'])) { #when a image is requested set type to Image
        echo "<meta property=\"og:type\" content=\image \"/>\n";
    } else { #if no image requested set type to Website
        echo "<meta property=\"og:type\" content=\"website\">\n";
    }
    if (isset($_GET['i'])) { #check do a image is requested
        if (file_exists($path . $_GET['i']) && !in_array($path . $_GET['i'], $unallowed)) { #if image exist and is not forbidden set requested image to Preview Picrute
            $hotlink = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . $path . $_GET['i'];#create the link for preview
        } elseif (file_exists($path . $_GET['i']) && in_array($path . $_GET['i'], $unallowed)) {  #When image is forbidden give forbidden picture
            $hotlink = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . $path . $forbiddenPicture;#create the link for preview
        } else { #When picture don´t exist give error Picture
            $hotlink = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . $path . $errorPicture;#create the link for preview
        }
    } else { #When only the domain is requested give logo
        $hotlink = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . $path .$previewPicture;#create the link for preview
    }
    foreach ($_GET as $key => $value) {
        $hotlink = str_replace('?' . $key . '=' . $value, "", $hotlink);
        $hotlink = str_replace('&' . $key . '=' . $value, "", $hotlink);
        $hotlink = str_replace('?' . $key, "", $hotlink);
        $hotlink = str_replace('&' . $key, "", $hotlink);
    } #get the values out of the link
    echo "<meta property=\"og:image\" content=\"".$hotlink."\"/>\n"; #echo the preview meta tag
    
    ?>
</head>
<body>
<?php
if (!in_array($_SERVER['REMOTE_ADDR'], $blockedIps)) { #Check do ip is not blocked
    
    echo "<a href=\"" . php_script_filename() . "\" title=\"Zur Frontpage\"><h1>Startseite</h1></a>\n";
    
    if (!file_exists($path . 'index.php')) { #When the path to upload and the index.php don't exist create
        $path_array = explode("/", $path); #Explode the path at /'s
        array_pop($path_array);
        foreach ($path_array as $key => $path_info) {
            if (empty($complete_path)) { #when the path to create folders is empty create
                $complete_path = $path_info . '/';
            } else { #When it's exist add the next thing to path
                $complete_path .= $path_info . '/';
            }
            if (!file_exists($complete_path)) { #If the folder where it's current create it
                if (!mkdir($complete_path, 0777, true)) { #If create folder was not succsesfull send error
                    #eventuelle error nachtich
                    $msg->error("Uerwarteter fehler, key => \t" . $key);
                }
            }
        }
        if (!file_exists($path . 'index.php')) { #When the index.php don't exist create index.php
            file_put_contents($path . 'index.php', "<?php\nheader(\"HTTP/1.1 403 Forbidden\");\nexit();");
        }
    }
    
    if (!empty($_GET['i'])) { #When a image is via i requested, do this
        $newPath = $path . $_GET['i']; #Set the image thats shown with the Path and the i was get
        if (file_exists($newPath) && !in_array($newPath, $unallowed)) { #When the file exist and is not unallowed then give informations
            $img_name = $_GET['i']; #The image name is the via get requested
            $info_link = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; #Create info link, info link is the link that show all this links
            foreach ($_GET as $key => $value) {
                $info_link = str_replace('?' . $key . '=' . $value, "", $info_link);
                $info_link = str_replace('&' . $key . '=' . $value, "", $info_link);
                $info_link = str_replace('?' . $key, "", $info_link);
                $info_link = str_replace('&' . $key, "", $info_link);
            }
            $info_link .= '?i=' . $img_name; #attent the ?i to the info link
            $hotlink = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . $newPath;#create the hotlink
            foreach ($_GET as $key => $value) {
                $hotlink = str_replace('?' . $key . '=' . $value, "", $hotlink);
                $hotlink = str_replace('&' . $key . '=' . $value, "", $hotlink);
                $hotlink = str_replace('?' . $key, "", $hotlink);
                $hotlink = str_replace('&' . $key, "", $hotlink);
            }
            $info_link = str_replace(php_script_filename(), "", $info_link); #Get the index.php out of url
            $hotlink = str_replace(php_script_filename(), "", $hotlink); #Get the index.php out of url
            $out['hotlink'] = "<p>Direktlink (Hotlink) zum Bild \t\t<input type=\"text\" value=\"" . $hotlink . "\" size=\"" . strlen($hotlink) . "\"></p>"; #set the hotlink into the out array
            $out['info_link'] = "<p>Infolik zum Bild(Anzeige aller Links) \t\t<input type=\"text\" value=\"" . $info_link . "\" size=\"" . strlen($info_link) . "\"></p>"; #set the info_link into the out array
            $out['bb_code_with'] = "<p>BB_code mit verlinkung auf den Info Link(Anzeige aller Links)\t<input type=\"text\" value=\"[url=" . $info_link . "][img]" . $hotlink . "[/img][/url]\" size=\"" . (strlen($info_link) + strlen($hotlink) + 5) . "\"></p>"; #set the bb_code:with_link into the out array
            $out['bb_code_without'] = "<p>BB_code ohne verlinkung auf den Info Link(Anzeige aller Links)\t<input type=\"text\" value=\"[img]" . $hotlink . "[/img]\"  size=\"" . (strlen($hotlink) + 1) . "\"></p>"; #set the bb_code_without into the out array
            $out['img'] = '<a href="' . $hotlink . '" title="' . $hotlink . '"><img class="img-show" src="' . $hotlink . '"></a></div>'; #Set the img html code into the out array
            echo $out['hotlink']; #echo the Hotlink
            echo $out['info_link']; #echo the info link
            echo $out['bb_code_with']; #echo the bb_code_with link
            echo $out['bb_code_without']; #echo the  bb_code_without link
            echo $out['img']; #echot the img html code
        } else { #When the requested picture don´t exist ore is blocked don´t show it
            $msg->error("Das gewünschte Dokument existiert leider nicht!");
        }
    } else { #When no image is requested then give out the Form
        echo $form; #echo Form
        if (isset($_FILES['img'])) { #when the form was send do this
            if ($useRecaptcha) { #Run the following code only when recaptcha should used
                if (!empty($_POST['g-recaptcha-response'])) { #If the recatcha response if filles
                    $captcha = $_POST['g-recaptcha-response']; #fetch the response
                    $url = "https://www.google.com/recaptcha/api/siteverify?secret=" . $recaptchaSecret . "&response=" . $captcha; #prepare the url for the recaptcha check
                    $curl = curl_init($url); #init curl
                    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); #Set the CURLOPT_FOLLOWLOCATION optin on true
                    curl_setopt($curl, CURLOPT_FRESH_CONNECT, true); #Set CURLOPT_FRESH_CONNECT optin on true
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); #Set the CURLOPT_RETURNTRANSFER on true
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); #Set the CURLOPT_SSL_VERIFYPEER on true
                    $json = json_decode(utf8_decode(curl_exec($curl)), true); #Execute the curl request and decode the Json for check
                    if ($json['success'] == true) { #check the succses from the request
                        $recaptchaVerify = true; #set the verify of the recaptcha on true
                    } else { #When succses is false give error and set verify var on false
                        create_log("recaptcha", "failed recaptcha fromm ip " . $_SERVER['REMOTE_ADDR']); #when a recaptcha solve was failed create log
                        $msg->error("Bitte löse das recaptch erneut!");
                        $recaptchaVerify = false;
                    }
                } else {#If the recaptcha not solved give error and set verify var on false
                    create_log("recaptcha", "failed recaptcha fromm ip " . $_SERVER['REMOTE_ADDR']); #when a recaptcha solve was failed create log
                    $msg->error("Bitte löse zuerst das recaptcha!");
                    $recaptchaVerify = false;
                }
            } else {#If recaptcha not ued set the cerify var on true
                $recaptchaVerify = true;
            }
            if ($recaptchaVerify) { #if verrify var true do this
                if (!empty($_FILES['img']['name'])) { #if a file was uploadet go forward
                    $file_extension = strtolower(pathinfo($_FILES['img']['name'], PATHINFO_EXTENSION)); #extrace the file extention
                    if (in_array($file_extension, $allowedTypes)) { #check do file extension is allowed
                        if ($_FILES['img']['size'] < $max_filesize) { #check do file is not to large
                            if (((get_dir_size($path) / 1048576) + $_FILES['img']['size']) >= $maxImgStorage) {
                                $name = randomstring($newNameLength) . '.' . $file_extension; #generate new, random filename with file extention
                                $newPath = $path . $name; #put together the new path
                                while (file_exists($newPath)) {#when filename already exist create
                                    $name = randomstring($newNameLength);
                                    $newPath = $path . $name;
                                }
                                if (move_uploaded_file($_FILES['img']['tmp_name'], $newPath)) { #if all fine move the file.
                                    $inUpload = true; #setInUpload to true
                                    $img_name = $name; #set the image name to the name
                                    $info_link = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; #Create info link, info link is the link that show all this links
                                    foreach ($_GET as $key => $value) {
                                        $info_link = str_replace('?' . $key . '=' . $value, "", $info_link);
                                        $info_link = str_replace('&' . $key . '=' . $value, "", $info_link);
                                        $info_link = str_replace('?' . $key, "", $info_link);
                                        $info_link = str_replace('&' . $key, "", $info_link);
                                    }
                                    $hotlink = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . $newPath;#create the hotlink
                                    foreach ($_GET as $key => $value) {
                                        $hotlink = str_replace('?' . $key . '=' . $value, "", $hotlink);
                                        $hotlink = str_replace('&' . $key . '=' . $value, "", $hotlink);
                                        $hotlink = str_replace('?' . $key, "", $hotlink);
                                        $hotlink = str_replace('&' . $key, "", $hotlink);
                                    }
                                    $info_link .= '?i=' . $img_name; #attent the ?i to the info link
                                    $info_link = str_replace(php_script_filename(), "", $info_link); #Get the index.php out of url
                                    $hotlink = str_replace(php_script_filename(), "", $hotlink); #Get the index.php out of url
                                    $out['hotlink'] = "<p>Direktlink (Hotlink) zum Bild \t\t<input type=\"text\" value=\"" . $hotlink . "\" size=\"" . strlen($hotlink) . "\"></p>"; #set the hotlink into the out array
                                    $out['info_link'] = "<p>Infolik zum Bild(Anzeige aller Links) \t\t<input type=\"text\" value=\"" . $info_link . "\" size=\"" . strlen($info_link) . "\"></p>"; #set the info_link into the out array
                                    $out['bb_code_with'] = "<p>BB_code mit verlinkung auf den Info Link(Anzeige aller Links)\t<input type=\"text\" value=\"[url=" . $info_link . "][img]" . $hotlink . "[/img][/url]\" size=\"" . (strlen($info_link) + strlen($hotlink) + 5) . "\"></p>"; #set the bb_code:with_link into the out array
                                    $out['bb_code_without'] = "<p>BB_code ohne verlinkung auf den Info Link(Anzeige aller Links)\t<input type=\"text\" value=\"[img]" . $hotlink . "[/img]\"  size=\"" . (strlen($hotlink) + 1) . "\"></p>"; #set the bb_code_without into the out array
                                    $out['img'] = '<div class="out-image"><a href="' . $hotlink . '" title="' . $hotlink . '"><img class="img-show" src="' . $hotlink . '"></a></div>'; #Set the img html code into the out array
                                    echo $out['hotlink']; #echo the Hotlink
                                    echo $out['info_link']; #echo the info link
                                    echo $out['bb_code_with']; #echo the bb_code_with link
                                    echo $out['bb_code_without']; #echo the  bb_code_without link
                                    echo $out['img']; #echot the img html code
                                } else { #when a error while move file give error
                                    $msg->error("ERROR");
                                }
                            } else {
                                $msg->error("Der angegebene Speichereplatz ist voll. Der Serveradministrator wurde verständigt.");
                                mail($_SERVER['SERVER_ADMIN'], "Storage is full", 'Hello dear Server admin. <br>At ' . date('l jS \of F Y h:i:s A') . " the script detected that the storage that you has configured is full! You should have a look on this!<b> Best regards,<br> the dev of myhost <br><br>When you think thats issue in the script, then please create a issue on github (<a href=\"https://github.com/cripiLP/myhost/issues/new\" title=\"Create new issue opn Github\">https://github.com/cripiLP/myhost/issues/new</a>). When you need help with myhostConfig then you can contact me via <a href=\"mailto:cripi1000@gmail.com\" title=\"Send ma a mail\">cripi1000@gmail.com</a> ore in future at <a href=\"http://contact.cripi.tk/myhost\" title=\"Contact form\">contact.cripi.tk/myhost</a>", "Fromm: Script@" . $_SERVER['HTTP_HOST'] . " <script@" . $_SERVER['HTTP_HOST'] . ">");
                            }
                        } else { #when file is to big give error
                            $msg->error("die Datei überschreibt die Maximal zulässige Dateigröße von " . $maxFilesizeMB . "MB");
                        }
                    } else { #when file is to big then give error
                        $msg->error("Bitte lade nur Bilddateien hoch!");
                    }
                } else { #Please coose data to upload
                    $msg->error("Bitte wähle eine Datei zum Hochladen aus!");
                }
            }
        }
    }
    if ($inUpload) { #when in upload create specific logs
        create_log("uploaded", "Image name: " . $name);
    }
    create_log("connection"); #create connection log
} else { #When ip blocked do this
    echo "<h1>Accses denied!</h1>"; #echo error message
    echo "<h4>403 Error</h4>";
    create_log("accses_denied", "Accses fromm ip " . $_SERVER['REMOTE_ADDR'] . " denied."); #create log that accses was denied
    die(); #Stopping script
}
} else {
    header("HTTP/1.1 500 Internal server error");
    mail($_SERVER['SERVER_ADMIN'], "License not accepted", "Today at " . date('l jS \of F Y h:i:s A') . " the script detected that you didn´t accepted the License. The Script works only if you accepted the license", "Fromm: Script <script@" . $_SERVER['HTTP_HOST'] . ">"); #Send the mail to the serer owner when the license is not accepted
    die("<h1>500 Error | Internal serve error</h1>");
}
?>
</body>
</html>
