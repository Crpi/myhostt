<?php
function php_script_filename()
{
    $arr = explode("/", $_SERVER['PHP_SELF']);
    $arr = array_reverse($arr);
    return $arr['0'];
}

function create_log($string, $description = null)
{
    if ($_SERVER['REMOTE_ADDR'] != '::1') {
        $ip = $_SERVER['REMOTE_ADDR'];
    } else {
        $ip = '127.0.0.1';
    }
    /*$ip = explode('.', $ip);
    unset($ip['3']);
    $ip = implode('.', $ip);*/
    #echo $ip;
    $url = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $date = date('l jS \of F Y h:i:s A');
    $date_ms = time();
    $logdate = date("d.m.Y");
    if (!empty($_SERVER["HTTP_REFERER"])) {
        $reffer = "ueber: " . $_SERVER["HTTP_REFERER"];
    } else {
        $reffer = "Direktaufruf";
    }
    if (!is_null($description)) {
        $zeile = $ip . "|" . $url . "|" . $date . " (" . $date_ms . ")" . "|" . $reffer . " - " . $_SERVER['HTTP_USER_AGENT'] . "|" . $description . "\n";
    } else {
        $zeile = $ip . "|" . $url . "|" . $date . " (" . $date_ms . ")" . "|" . $reffer . "|" . $_SERVER['HTTP_USER_AGENT'] . "\n";
    }
    $file_name = "logs/" . date("Y") . "/" . date("m") . "/" . date('d') . "/" . $string . "_log_" . $logdate . ".log";
    if (file_exists("logs/" . date("Y") . "/" . date("m") . "/" . date("d") . '/')) {
        file_put_contents($file_name, $zeile, FILE_APPEND);
    } elseif (file_exists("logs/" . date("Y") . "/" . date("m") . "/")) {
        mkdir("logs/" . date("Y") . "/" . date("m") . "/" . date("d") . "/");
        create_log($string, $description);
    } elseif (file_exists("logs/" . date('Y') . "/")) {
        mkdir("logs/" . date("Y") . "/" . date("m") . "/");
        mkdir("logs/" . date("Y") . "/" . date("m") . "/" . date("d") . "/");
        create_log($string, $description);
    } elseif (file_exists('logs/')) {
        mkdir("logs/" . date("Y") . "/");
        mkdir("logs/" . date("Y") . "/" . date("m") . "/");
        mkdir("logs/" . date("Y") . "/" . date("m") . "/" . date("d") . "/");
        create_log($string, $description);
    } else {
        mkdir('logs/');
        mkdir("logs/" . date("Y") . "/");
        mkdir("logs/" . date("Y") . "/" . date("m") . "/");
        mkdir("logs/" . date("Y") . "/" . date("m") . "/" . date("d") . "/");
        create_log($string, $description);

    }
    $return = $date;
    return $return;
}

function randomstring($length, $possible_chars = "ABCDEFGHIJKLMOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890")
{
    $letters = $possible_chars;
    $letters_count = strlen($letters) - 1;
    $length--;
    $string = $letters[mt_rand(0, $letters_count)];
    for ($i = 0; $i < $length; $i++) {
        $string .= $letters[mt_rand(0, $letters_count)];
    }
    return $string;
}

function get_dir_size($directory){
    /*
     * Function by Alex Kashin on Stackoverflow.
     * http://stackoverflow.com/users/2424868/alex-kashin
     * http://stackoverflow.com/questions/478121/php-get-directory-size/27266353#27266353
     * Used only for short time, I try untill the next update to develop a own function!
     */
    $size = 0;
    $files= glob($directory.'/*');
    foreach($files as $path){
        is_file($path) && $size += filesize($path);
        is_dir($path) && get_dir_size($path);
    }
    return $size;
}
